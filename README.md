# Identicon

Generate an Identicon in a Github like fashion.
The generation is based on your input. 
Same input, same result.

## Installation

You'll need to download Elixir -> [Elixir installation]

Then open up a terminal and type :
```
    $ mix deps.get
```

After that, type :
```
    $ iex -S mix
```

You'll be in the Elixir REPL, like node or irb for JS or Ruby.
To test it out, type :
```
    $ Identicon.main("foo")
```

To generate a foo.png at the root of the project

[Elixir installation]: https://elixir-lang.org/install.html
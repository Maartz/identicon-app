defmodule Identicon do
  @moduledoc """
  Create an identicon based on a string input
  """

  def main(input) do
    input
    |> hash_input
    |> pick_color
    |> build_grid
    |> filter_odd_squares
    |> build_pixel_map
    |> draw_image
    |> save_image(input)
  end

  @doc """
    Takes a string input and hash it through md5 crypto and output it as a struct.
  """
  def hash_input(input) do
    hex = :crypto.hash(:md5, input)
    |> :binary.bin_to_list
    %Identicon.Image{hex: hex}
  end

  @doc """
    Takes a struct and give the 3 first values back to a tuple in the II struct.
  """
  def pick_color(%Identicon.Image{hex: [r, g, b | _rest]} = image) do
    # Create an image struct and assign a tuple to the color key
    %Identicon.Image{image | color: {r,g,b}}
  end

  def build_grid(%Identicon.Image{hex: hex} = image) do
    grid =
      hex
      |> Enum.chunk(3) # TODO: Update chunk function (deprecated)
      |> Enum.map(&mirror_row/1) # Reference a func with & and set num of params with /1
      |> List.flatten
      |> Enum.with_index # Add indexes
    %Identicon.Image{image | grid: grid} # assign grid in the II struct at grid key
  end

  @doc """
    Takes a list of 3 and process it to be symmetrical
  """
  def mirror_row(row) do
    [first, second | _rest] = row # Pattern matching
    row ++ [second, first] # Concatenation
  end

  @doc """
    Takes the grid value of II struct and filter the odd number out of it
  """
  def filter_odd_squares(%Identicon.Image{grid: grid} = image) do
    grid = Enum.filter grid, fn ({code, _index}) ->
      rem(code, 2) == 0 # Calculate the remainder
    end

    %Identicon.Image{image | grid: grid}
  end

  def build_pixel_map(%Identicon.Image{grid: grid} = image)do
    pixel_map =
      Enum.map grid, fn ({_value, index}) ->
        # Top left point and bottom right calculating
        horizontal = rem(index, 5) * 50
        vertical = div(index, 5) * 50
        top_left = {horizontal, vertical}
        bottom_right = {horizontal + 50, vertical + 50}
        {top_left, bottom_right}
      end
    %Identicon.Image{image | pixel_map: pixel_map}
  end

  @doc """
    Draw a square of 250 x 250 with EGD Erlang
    http://erlang.org/documentation/doc-6.1/lib/percept-0.8.9/doc/html/egd.html
  """
  def draw_image(%Identicon.Image{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)

    Enum.each pixel_map, fn({start, stop})  ->
      :egd.filledRectangle(image, start, stop, fill)
    end

    :egd.render(image)
  end

  def save_image(image, input) do
    File.write("#{input}.png", image)
  end

end
